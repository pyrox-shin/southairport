# 考察結束後...

相信這趟旅程結束以後，一定讓你開了不少眼界，也讓你有很多想法或情緒上的感受。

在實地的考察中，除了理性的觀察、紀錄與分析，記錄下自己當下的情緒與五感與想法也是很重要的。想要大家想一些簡單的問題：
1. 這趟旅程下來，你最大的收穫或心得是甚麼？
2. 這趟旅程過後，你覺得自己跟南機場（田野）建立了怎樣的關係？

這些問題會被整合到這個表單當中：[城鄉組南機場考察問題](https://forms.gle/nX6KoKYwU283crRE6)，也歡迎在最後給我們回饋！

## 參考文獻

方億玲（2015）。**【讀者投書】南機場的忠義國小，不該成為都更犧牲品** ，獨立評論。2023年7月取自：https://opinion.cw.com.tw/blog/profile/52/article/2621

南機場夜市自治會（2022）。**關於南機場**。2023年7月取自：https://reurl.cc/b9Anlr

陳德君（2015）。**【讀者投書】柯P南機場公辦都更好事一樁 為何卡住？** 獨立評論。2023年7月取自：https://opinion.cw.com.tw/blog/profile/52/article/3110

吳柏緯（2014）。**現代住宅計畫中的都市修補術—街道市場於南機場國宅群落的社會空間角色**。國立臺灣大學建築與城鄉研究所碩士論文。

胡皓瑋（2013）。**都市居住的社會排除與包容：以臺北市南機場整建住宅社區為例**。國立臺灣大學建築與城鄉研究所碩士論文。

周素卿、劉美琴（2001）。**都市更新視域外的性別、遷移與貧民窟世界——以臺北市忠勤社區女性遷入者的經驗為例**。地理學報，30：19-53。

國家文化記憶庫（2021）。**南機場國民住宅的歷史**。2023年7月取自：https://cmsdb.culture.tw/object/7ECE869B-373C-49EE-8C43-8F5BBBED7907

臺北市政府（2021）。**訂定臺北市中正區-2-捷運LG03站暨南機場整建住宅及周邊更新地區（含萬華-7 更新地區）都市更新計畫案**。2023年7月取自：https://reurl.cc/2LVo3n

臺北市政府（2018）。**擬定臺北市中正區永昌段四小段633-5地號等15筆土地及萬華區青年段一小段126地號等2筆土地都市更新計畫暨配合擴大劃定永昌段四小段645地號部分土地為更新地區**。2023年7月取自：https://reurl.cc/QXxj3p
